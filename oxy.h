/*
 * © 2021 Zander Brown <zbrown@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * 
 * Author: Zander Brown <zbrown@gnome.org>
 */

#pragma once

#include "min-fancy-object.h"

G_BEGIN_DECLS

#define OXY_TYPE_VERY_FANCY_OBJECT oxy_very_fancy_object_get_type ()

G_DECLARE_DERIVABLE_TYPE (OxyVeryFancyObject, oxy_very_fancy_object, OXY, VERY_FANCY_OBJECT, MinFancyObject)


struct _OxyVeryFancyObjectClass {
  MinFancyObjectClass parent_class;
};


double oxy_very_fancy_object_get_total_score (OxyVeryFancyObject *self);
void   oxy_very_fancy_object_add_object      (OxyVeryFancyObject *self,
                                              MinFancyObject     *obj,
                                              guint               data);
void   oxy_very_fancy_object_remove_object   (OxyVeryFancyObject *self,
                                              MinFancyObject     *obj);
guint  oxy_very_fancy_object_lookup_object   (OxyVeryFancyObject *self,
                                              MinFancyObject     *obj);

G_END_DECLS
