/*
 * © 2021 Zander Brown <zbrown@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Zander Brown <zbrown@gnome.org>
 */

#define G_LOG_DOMAIN "Min"

#include "min-fancy-object.h"

enum {
  PROP_0,
  PROP_HOW_FANCY,
  LAST_PROP
};
static GParamSpec *props[LAST_PROP];


typedef struct _MinFancyObjectPrivate MinFancyObjectPrivate;
struct _MinFancyObjectPrivate {
  char *how_fancy;
};

G_DEFINE_TYPE_WITH_PRIVATE (MinFancyObject, min_fancy_object, G_TYPE_OBJECT)


static void
min_fancy_object_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  MinFancyObject *self = MIN_FANCY_OBJECT (object);
  MinFancyObjectPrivate *priv = min_fancy_object_get_instance_private (self);

  switch (property_id) {
    case PROP_HOW_FANCY:
      g_clear_pointer (&priv->how_fancy, g_free);
      priv->how_fancy = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}


static void
min_fancy_object_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  MinFancyObject *self = MIN_FANCY_OBJECT (object);
  MinFancyObjectPrivate *priv = min_fancy_object_get_instance_private (self);

  switch (property_id) {
    case PROP_HOW_FANCY:
      g_value_set_string (value, priv->how_fancy);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}


static void
min_fancy_object_finalize (GObject *object)
{
  MinFancyObject *self = MIN_FANCY_OBJECT (object);
  MinFancyObjectPrivate *priv = min_fancy_object_get_instance_private (self);

  g_clear_pointer (&priv->how_fancy, g_free);

  G_OBJECT_CLASS (min_fancy_object_parent_class)->finalize (object);
}


static void
min_fancy_object_class_init (MinFancyObjectClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = min_fancy_object_finalize;
  object_class->set_property = min_fancy_object_set_property;
  object_class->get_property = min_fancy_object_get_property;

  props[PROP_HOW_FANCY] = g_param_spec_string ("how-fancy",
                                               "How Fancy",
                                               "How Fancy is this object?",
                                               "fancy",
                                               G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);


  g_object_class_install_properties (object_class, LAST_PROP, props);
}


static void
min_fancy_object_init (MinFancyObject *self)
{
  MinFancyObjectPrivate *priv = min_fancy_object_get_instance_private (self);

  priv->how_fancy = g_strdup ("fancy");
}
