//
// © 2021 Zander Brown
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Author: Zander Brown <zbrown@gnome.org>
//

pub use ffi;

#[allow(unused_imports)]
#[allow(clippy::let_and_return)]
#[allow(clippy::type_complexity)]
mod auto;

#[macro_use]
pub mod subclass;

pub use auto::*;
