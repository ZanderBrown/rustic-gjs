//
// © 2021 Zander Brown
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Author: Zander Brown <zbrown@gnome.org>
//

use crate::subclass::prelude::*;

use glib::Object;

use crate::FancyObject;

pub trait FancyObjectImpl: FancyObjectImplExt + ObjectImpl {

}

pub trait FancyObjectImplExt: ObjectSubclass {
}

impl<T: FancyObjectImpl> FancyObjectImplExt for T {
}

unsafe impl<T: FancyObjectImpl> IsSubclassable<T> for FancyObject {
    fn class_init(class: &mut glib::Class<Self>) {
        <Object as IsSubclassable<T>>::class_init(class);
    }

    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <Object as IsSubclassable<T>>::instance_init(instance);
    }
}
