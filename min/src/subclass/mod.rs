//
// © 2021 Zander Brown
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Author: Zander Brown <zbrown@gnome.org>
//

pub mod fancy_object;

pub mod prelude {
    #[doc(hidden)]
    pub use glib::subclass::prelude::*;

    pub use super::fancy_object::{FancyObjectImpl, FancyObjectImplExt};
}
