/*
 * © 2021 Zander Brown <zbrown@gnome.org>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * 
 * Author: Zander Brown <zbrown@gnome.org>
 */

#pragma once

#include <glib-object.h>

#include "min-enums.h"

G_BEGIN_DECLS

typedef enum /*< enum >*/ {
  MIN_MAGIC_VALUE_A,
  MIN_MAGIC_VALUE_B,
} MinMagicValue;


#define MIN_TYPE_FANCY_OBJECT min_fancy_object_get_type ()

G_DECLARE_DERIVABLE_TYPE (MinFancyObject, min_fancy_object, MIN, FANCY_OBJECT, GObject)


struct _MinFancyObjectClass {
  GObjectClass parent_class;
};

G_END_DECLS
