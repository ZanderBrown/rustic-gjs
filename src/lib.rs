//
// © 2021 Zander Brown
//
// SPDX-License-Identifier: GPL-3.0-or-later
//
// Author: Zander Brown <zbrown@gnome.org>
//

use glib::gobject_ffi;
use glib::prelude::*;
use glib::subclass::prelude::*;
use glib::translate::*;

use std::cell::RefCell;
use std::collections::HashMap;

mod imp {
    use min::{FancyObject, subclass::prelude::*, traits::FancyObjectExt};

    use super::*;

    pub struct VeryFancyObject {
        pub(super) score: RefCell<f64>,
        pub(super) magic: RefCell<min::MagicValue>,
        pub(super) map: RefCell<HashMap<min::FancyObject, u32>>,
    }

    impl Default for VeryFancyObject {
        fn default() -> Self {
            Self {
                score: RefCell::new(42.0),
                magic: RefCell::new(min::MagicValue::A),
                map: RefCell::new(HashMap::new()),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VeryFancyObject {
        const NAME: &'static str = "OxyVeryFancyObject";

        type Instance = CVeryFancyObject;
        type Type = super::VeryFancyObject;
        type ParentType = FancyObject;

        type Interfaces = ();
    }

    impl ObjectImpl for VeryFancyObject {
        fn properties() -> &'static [glib::ParamSpec] {
            use once_cell::sync::Lazy;
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpec::new_double(
                        "score",
                        "Score",
                        "Score",
                        0.0,
                        f64::INFINITY,
                        42.0,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::EXPLICIT_NOTIFY,
                    ),
                    glib::ParamSpec::new_enum(
                        "magic",
                        "Magic",
                        "Magic",
                        min::MagicValue::static_type(),
                        min::MagicValue::A.into_glib(),
                        glib::ParamFlags::READWRITE,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "score" => {
                    let score = value.get().expect("score double");
                    if self.score.replace(score) == score {
                        return;
                    }
                    obj.notify("score");
                }
                "magic" => {
                    self.magic.replace(value.get().expect("magic enum"));
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "score" => self.score.borrow().to_value(),
                "magic" => self.magic.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.set_how_fancy(Some("very"));
        }
    }

    impl FancyObjectImpl for VeryFancyObject {}
}

#[repr(C)]
pub struct CVeryFancyObject {
    parent: gobject_ffi::GObject,
}

unsafe impl InstanceStruct for CVeryFancyObject {
    type Type = imp::VeryFancyObject;
}

impl From<*const CVeryFancyObject> for VeryFancyObject {
    fn from(ptr: *const CVeryFancyObject) -> Self {
        unsafe { &*ptr }.impl_().instance()
    }
}

glib::wrapper! {
    pub struct VeryFancyObject(ObjectSubclass<imp::VeryFancyObject>) @extends min::FancyObject;
}

impl VeryFancyObject {
    pub fn total_score(&self) -> f64 {
        let self_ = imp::VeryFancyObject::from_instance(&self);

        self_
            .map
            .borrow()
            .keys()
            .map(|obj| {
                if let Ok(ref obj) = obj.clone().downcast() {
                    let self_ = imp::VeryFancyObject::from_instance(&obj);

                    *self_.score.borrow()
                } else {
                    0.0
                }
            })
            .sum()
    }

    pub fn add_object(&self, obj: &min::FancyObject, data: u32) -> bool {
        let self_ = imp::VeryFancyObject::from_instance(&self);

        if self_.map.borrow().contains_key(obj) {
            return false;
        }

        self_.map.borrow_mut().insert(obj.clone(), data);

        true
    }

    pub fn remove_object(&self, obj: &min::FancyObject) -> bool {
        let self_ = imp::VeryFancyObject::from_instance(&self);

        if let Some(_) = self_.map.borrow_mut().remove(obj) {
            true
        } else {
            false
        }
    }

    pub fn lookup_object(&self, obj: &min::FancyObject) -> u32 {
        let self_ = imp::VeryFancyObject::from_instance(&self);

        self_
            .map
            .borrow_mut()
            .get(obj)
            .map(|data| *data)
            .unwrap_or(0)
    }
}

#[no_mangle]
pub extern "C" fn oxy_very_fancy_object_get_type() -> glib::ffi::GType {
    VeryFancyObject::static_type().into_glib()
}

#[no_mangle]
pub unsafe extern "C" fn oxy_very_fancy_object_get_total_score(
    fancy: *const CVeryFancyObject,
) -> libc::c_double {
    VeryFancyObject::from(fancy).total_score()
}

#[no_mangle]
pub unsafe extern "C" fn oxy_very_fancy_object_add_object(
    fancy: *const CVeryFancyObject,
    obj: *mut min::ffi::MinFancyObject,
    data: libc::c_uint,
) -> glib::ffi::gboolean {
    VeryFancyObject::from(fancy)
        .add_object(&from_glib_none(obj), data)
        .into()
}

#[no_mangle]
pub unsafe extern "C" fn oxy_very_fancy_object_remove_object(
    fancy: *const CVeryFancyObject,
    obj: *mut min::ffi::MinFancyObject,
) -> glib::ffi::gboolean {
    VeryFancyObject::from(fancy)
        .remove_object(&from_glib_none(obj))
        .into()
}

#[no_mangle]
pub unsafe extern "C" fn oxy_very_fancy_object_lookup_object(
    fancy: *const CVeryFancyObject,
    obj: *mut min::ffi::MinFancyObject,
) -> libc::c_uint {
    VeryFancyObject::from(fancy)
        .lookup_object(&from_glib_none(obj))
        .into()
}
