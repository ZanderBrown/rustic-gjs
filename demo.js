#!/usr/bin/gjs

const Gi = imports.gi.GIRepository;

Gi.Repository.prepend_library_path('./out')
Gi.Repository.prepend_search_path('./out')

const { Min, Oxy, GObject } = imports.gi;

const fancy = new Min.FancyObject();
log(fancy.how_fancy);

const very = new Oxy.VeryFancyObject();
very.add_object(fancy, 123);
very.add_object(fancy, 234);
very.score = 23.4;
log(very.how_fancy);

var SuperFancyObject = GObject.registerClass({
    Properties: {
        'some-prop': GObject.ParamSpec.boolean(
            'some-prop', 'some-prop', 'some-prop',
            GObject.ParamFlags.READWRITE,
            true),
    },
},
    class SuperFancyObject extends Oxy.VeryFancyObject {
        _init(magic) {
            super._init({
                magic
            });

            this.how_fancy = "super";
        }

        get someProp() {
            return this._someProp;
        }

        set someProp(enabled) {
            this._someProp = enabled;
        }
    });

const sup = new SuperFancyObject(Min.MagicValue.B);
sup.add_object(very, 345);
log(sup.how_fancy);

const another = new Oxy.VeryFancyObject();
another.score = 1.234;
sup.add_object(another, 567);

log(sup.get_total_score());

log(sup.lookup_object(another));

sup.remove_object(another);

log(sup.get_total_score());
