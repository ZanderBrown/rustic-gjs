#!/usr/bin/bash

set -ex

mkdir -p out

glib-mkenums --output out/min-enums.c --template=min-enums.c.in min-fancy-object.h
glib-mkenums --output out/min-enums.h --template=min-enums.h.in min-fancy-object.h

gcc                                                         \
    -fPIC -I./out -I.                                       \
    `pkg-config --libs --cflags gobject-2.0`                \
    -shared -o out/libfancy.so                              \
    min-fancy-object.c                                      \
    out/min-enums.c

export PKG_CONFIG_PATH=$PWD

g-ir-scanner                                                \
    --no-libtool                                            \
    --warn-all                                              \
    --quiet                                                 \
    -L./out --library=fancy                                 \
    -n Min --nsversion 1.0                                  \
    --include GObject-2.0                                   \
    --output out/Min-1.0.gir                                \
    --pkg=min-1.0                                           \
    --pkg-export=min-1.0                                    \
    min-fancy-object.h out/min-enums.h
g-ir-compiler                                               \
    --shared-library=out/libfancy.so                        \
    --output out/Min-1.0.typelib                            \
    --includedir ./out                                      \
    out/Min-1.0.gir

cd min/sys
gir -d ../../out -d ../../girs
cd ..
gir -d ../out -d ../girs
cd ..

cargo build

cp target/debug/liboxide.so out

 g-ir-scanner                                               \
    --no-libtool                                            \
    --warn-all                                              \
    --quiet                                                 \
    -L./out --library=oxide                                 \
    -n Oxy --nsversion 1.0                                  \
    --include GObject-2.0                                   \
    --add-include-path ./out                                \
    --pkg=min-1.0                                           \
    --pkg=oxy-1.0                                           \
    --pkg-export=oxy-1.0                                    \
    --include Min-1.0                                       \
    --output out/Oxy-1.0.gir                                \
    oxy.h
g-ir-compiler                                               \
    --shared-library=out/liboxide.so                        \
    --output out/Oxy-1.0.typelib                            \
    --includedir ./out                                      \
    out/Oxy-1.0.gir
