# C + Rust + GJS

An example of C + Rust & GJS playing nicely with each other

build-it.sh to build everything
run-it.sh to run the demo

## Dependencies

- gobject-intospection
- Rust/Cargo
- [`gir`](github.com/gtk-rs/gir) *might be renamed*
